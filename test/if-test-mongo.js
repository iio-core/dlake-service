const runTest = require('./if-test').run
const populate = require('../tools/populate_db-mongo.js')

async function run() {
  await populate()
  runTest()
}

run()
