const should = require('should')
const chalk = require('chalk')

const Gateway = require('@ignitial/iio-services').Gateway
const config = require('../config')

const rolesData = require('../tools/data/roles')
const usersData = require('../tools/data/users')

function run() {
  let gateway = new Gateway(config)
  gateway._init()

  gateway._waitForService('dlake', 10000).then(async dlake => {
    let cfg = await dlake.dbCfg()
    console.log(chalk.green('wait for service: service up ✔'))
    dlake.users.find({}, '5b0d843b2a7f1d105348dc96').then(docs => {
      (docs.length >= 2).should.be.true()
      should.not.exist(docs[0].password)
      console.log(chalk.green('wait for service: find by unkown user ✔'))
    }).catch(err => {
      console.log(chalk.red('wait for service: find by unkown user ✘'), '' + err)
    })
  }).catch(err => {
    console.log(chalk.red('wait for service ✘'), '' + err)
    gateway._dying('exiting...', err)
  })

  gateway.on('service:registered', service => {
    if (service.name === 'dlake') {
      let users = gateway.services.dlake.users
      should.exist(users)
      console.log(chalk.green('users service:registered ✔'))

      // test with anonymous
      users.find({}).then(docs => {
        should(docs.length).be.equal(2)
        should.not.exist(docs[0].contactInfo)
        should.not.exist(docs[0].password)
        should.exist(docs[0].lastname)
        console.log(chalk.green('find by anonymous user ✔'))

        let admin, stdUser

        for (let user of docs) {
          if (user.role === 'admin') admin = user
          if (user.role === 'user') stdUser = user
        }

        users.get({ _id: admin._id }, stdUser._id).then(doc => {
          console.log(chalk.green('get admin info by standard user ✔'))

          doc.lastname = 'Cutunio'

          users.put(doc, stdUser._id).then(doc => {
            console.log(chalk.red('not possible to set user info by standard user ✘'))
          }).catch(err => {
            console.log(chalk.green('not possible to set user info by standard user ✔'), '' + err)

            users.put(doc, admin._id).then(result => {
              if (result._updated && result._updated.nModified) {
                should(result._updated.nModified).be.equal(1)
                console.log(chalk.green('update user info by admin user ✔'))
              } else {
                should.exist(result._id)
                console.log(chalk.green('update user info by admin user ✔'))
              }
              users.get({ _id: doc._id }, stdUser._id).then(udoc => {
                should(udoc.lastname).be.equal('Cutunio')
                console.log(chalk.green('check updated info by standard user ✔'))
              }).catch(err => console.log(chalk.red('check updated info by standard user ✘'), '' + err))
            }).catch(err => console.log(chalk.red('update user info by admin user ✘'), '' + err))
          })
        }).catch(err =>
          console.log(chalk.red('get admin info by standard user ✘'), '' + err))

        users.get({ _id: admin._id }).then(doc => {
          should.not.exist(doc.contactInfo)
          should.exist(doc.lastname)
          console.log(chalk.green('get restricted admin info by anonymous user ✔'))
        }).catch(err =>
          console.log(chalk.red('get admin info by anonymous user ✘'), '' + err))

        users.findAndSort({ query: {}, sort: { username: -1 }}, admin._id).then(result => {
          should(result.docs[0].username).be.equal('tcrood')
          console.log(chalk.green('find and sort by admin user ✔'))
        }).catch(err =>
          console.log(chalk.red('find and sort by admin user ✘'), '' + err))

        users.findPaginated({}, admin._id).then(result => {
          should(result.docs.length).be.equal(2)
          console.log(chalk.green('find paginated by admin user ✔'))
        }).catch(err =>
          console.log(chalk.red('find paginated by admin user ✘'), '' + err))

        let roles = gateway.services.dlake.roles
        console.log(chalk.green('roles service:registered ✔'))

        roles.load().then(() => {
          console.log(chalk.green('roles load ✔'))
          roles.save(rolesData, admin._id).then(() => {
            console.log(chalk.green('roles save by admin ✔'))
          }).catch(err => {
            console.log(chalk.red('roles save by admin ✘'), '' + err)
          })

          users.find({}, admin._id).then(docs => {
            should(docs.length).be.equal(2)
            should.exist(docs[0].password)
            console.log(chalk.green('find by admin user ✔'))
          }).catch(err =>
            console.log(chalk.red('find by admin user ✘'), '' + err))

          users.find({}, stdUser._id).then(docs => {
            should(docs.length).be.equal(2)
            should.exist(docs[0].password)
            console.log(chalk.green('find by standard user ✔'))
          }).catch(err =>
            console.log(chalk.red('find by standard user ✘'), '' + err))

          let nu = usersData[0]
          nu.username = 'icrood'

          users.putNewUser(nu, stdUser._id).then(() => {
            console.log(chalk.red('create by standard user ✘'))
          }).catch(err => {
            console.log(chalk.green('not created by standard user ✔'), '' + err)

            users.putNewUser(nu, admin._id)
              .then(result => {
                should.exist(result.doc)
                console.log(chalk.green('create by admin user ✔'))

                users.del(result.doc).then(() => {
                  console.log(chalk.red('deleted by anonymous user ✘'))
                }).catch(err => {
                  console.log(chalk.green('not deleted by anonymous user ✔'), '' + err)

                  users.del(result.doc, admin._id).then(() => {
                    console.log(chalk.green('deleted by admin user ✔'))
                    gateway._dying('', '')
                  }).catch(err => {
                    console.log(chalk.red('deleted by admin user ✘'), '' + err)
                    gateway._dying('', '')
                  })
                })
              }).catch(err =>
                console.log(chalk.red('create by admin user ✘'), '' + err))
          })
        }).catch(err => {
          console.log(chalk.red('load roles ✘'), '' + err)
        })
      }).catch(err => {
        console.log(chalk.red('find by anonymous user ✘'), '' + err)
      })
    }
  })

  gateway.on('service:unregistered', service => {
    console.log('service unregistered', service)
  })
}

exports.run = run
