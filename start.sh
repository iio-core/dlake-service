#!/bin/sh

docker-compose up -d
sleep 1

npm run build:dev

export IIOS_SERVER_PORT=20013
export IIOS_NAMESPACE=ignitialio

node index.js
