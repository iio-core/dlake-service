const fs = require('fs')
const path = require('path')
const Mongo = require('./lib/mongo').Mongo
const Couch = require('./lib/couch').Couch
const nodemailer = require('nodemailer')

const pino = require('./lib/utils').pino

const Service = require('@ignitial/iio-services').Service
const utils = require('@ignitial/iio-services').utils
const config = require('./config')
const getAllMethods = utils.getMethods

const Item = require('./lib/core/item').Item

class DLake extends Service {
  constructor(options) {
    // set service name before calling super
    options.name = options.name || 'dlake'
    super(options)

    // datum references
    this.datum = {}

    let version = (JSON.parse(fs.readFileSync('package.json'))).version
    console.log('--- VERSION: ' + version + ' ---')

    switch (this._options.db.engine) {
      case 'mongo':
        this._mongo = new Mongo(this._options.mongo)
        break
      case 'couch':
        this._couch = new Couch(this._options.couch)
        break
    }

    // datum config
    this._datumOptions = {}

    // wait for service to be ready in order to initialize it
    this._waitForPropertySet('_ready', true).then(async () => {
      // after $app injectio into prototype, we can load datum(s)
      let datums = []

      for (let p of this._options.datum.paths) {
        if (p[0] !== '/') {
          p = path.join(__dirname, p)
        }

        try {
          fs.accessSync(p, fs.constants.F_OK)
          pino.info('add datum from folder', p)
          for (let f of fs.readdirSync(p)) {
            if (f.match(/\.datum\.js/)) {
              pino.info('add datum file', path.join(p, f))
              datums.push(path.join(p, f))
            } else {
              pino.warn('skipped file: not datum', path.join(p, f))
            }
          }

          if (this._options.datum.configPath) {
            let configPath = path.join(p, this._options.datum.configPath)

            try {
              this._datumOptions = {
                ...require(configPath)
              }

              pino.info('datum options updated', this._datumOptions)
            } catch (err) {
              pino.warn('configuration file [%s] does not exist', p, '' + err)
            }
          }
        } catch (err) {
          pino.warn('folder [%s] does not exist', p, '' + err)
        }
      }

      pino.info('datums list', JSON.stringify(datums, null, 2))

      // load datum instances
      for (let file of datums) {
        await this._addDatum(file)
      }

      // add bridge
      for (let d in this.datum) {
        let methods = getAllMethods(this.datum[d])

        for (let m of methods) {
          this[d + ':' + m] = (args, userId) => {
            pino.info(d + ':' + m + ' ', args, userId)
            return this.datum[d][m](args, userId)
          }
        }
      }

      // initialize the service (register into Redis dico)
      this._init().then(() => {
        // Create a SMTP transporter object
        if (options.emailer) {
          this._emailer = nodemailer.createTransport(options.emailer.smtp, options.emailer.mail)
        }

        pino.info('Service ' + this._name + ' declaration done')
      }).catch(err => {
        pino.error('Initialization failed >', err)
        process.exit(1)
      })
    }).catch(err => {
      pino.error('Service not ready on time >', err)
      process.exit(1)
    })
  }

  async _addDatum(file) {
    try {
      let m = require(file)

      for (let exported in m) {
        // inject datum reference for cross uses
        m[exported].prototype.$datum = this.datum
        // inject reference to app
        m[exported].prototype.$app = this

        // needs empty object if undefined
        this._datumOptions[exported] = this._datumOptions[exported] || {}
        // gets auth config
        this._datumOptions[exported].auth = this._options.auth
        // enable REST
        this._datumOptions[exported].enableREST = this._options.db.enableREST

        let instance = new m[exported](this._datumOptions[exported])
        this.datum[instance._name] = instance
        await instance._init()

        pino.info('datum [%s] loaded', instance._name)
      }
    } catch (err) {
      pino.error(err, 'datum load failed')
    }
  }

  addDatum(args) {
    return new Promise(async (resolve, reject) => {
      if (!this.datum[args.name]) {
        try {
          Item.prototype.$app = this
          Item.prototype.$datum = this.datum
          this.datum[args.name] = new Item({ name: args.name })
          await this.datum[args.name]._init()
        } catch (err) {
          reject(err)
          return
        }

        this.datum[args.name].ready().then(async () => {
          try {
            for (let d in this.datum) {
              let methods = getAllMethods(this.datum[d])

              for (let m of methods) {
                this[d + ':' + m] = (args, userId) => {
                  pino.info(d + ':' + m + ' ', args, userId)
                  return this.datum[d][m](args, userId)
                }
              }
            }

            await this._registerMethods()
          } catch (err) {
            pino.error('registering failed...')
          }

          if (args.roles) {
            for (let who in args.roles) {
              for (let which in args.roles[who]) {
                for (let what in args.roles[who][which]) {
                  try {
                    let target = what.split(':')[1]
                    let method = what.split(':')[0] + target.charAt(0).toUpperCase() + target.slice(1)

                    this.datum.roles._ac.grant(who)[method](which, args.roles[who][which][what])
                  } catch (err) {
                    reject(err)
                    pino.error('ac err', err)
                    return
                  }
                }
              }
            }

            pino.warn('datum [%s] dynamically loaded', args.name)

            this.datum.roles._save().then(() => {
              resolve()
            }).catch(err => {
              pino.error('save err', err)
              reject(err)
            })
          } else {
            resolve()
          }
        }).catch(err => {
          pino.error('ready err', err)
          reject(err)
        })
      } else {
        reject(new Error('datum [' + args.name + '] already defined'))
      }
    })
  }

  dbCfg() {
    return new Promise((resolve, reject) => {
      resolve(this._options[this._options.db.engine])
    })
  }

  restart() {
    return new Promise((resolve, reject) => {
      resolve()
      this._dying('restart requested')
    })
  }

  /* send mail on notification, for example (internal) */
  _sendMail(mailOptions) {
    return new Promise((resolve, reject) => {
      this._emailer.sendMail(mailOptions, (err, info) => {
        if (err) {
          pino.error(err, 'Mailer failed')
          reject(err)
        } else {
          resolve(info)
        }
      })
    })
  }

  /* i18n local management */
  _i18n(lang, what, ...params) {
    if (!this._options.i18n) return what
    let langIndex = 0

    for (let i = 0; i < this._options.i18n.lang.length; i++) {
      if (this._options.i18n.lang[i] === lang) {
        langIndex = i
        break
      }
    }

    if (langIndex === 0) {
      if (what && what.match(/`/) && what.length < 256) {
        return eval(what) // eslint-disable-line no-eval
      }
      return what
    }

    langIndex-- // reindex

    // BUG: some kind of rest params bug when eval
    params = params // eslint-disable-line no-self-assign

    if (this._options.i18n.translations[what]) {
      // protect against malicious code (even if internal)
      //   what.length < 256
      // To Be Improved
      if (what.match(/`/) && what.length < 256) {
        return eval(this._options.i18n.translations[what][langIndex]) // eslint-disable-line no-eval
      } else {
        return this._options.i18n.translations[what][langIndex]
      }
    } else {
      if (what && what.match(/`/) && what.length < 256) {
        return eval(what) // eslint-disable-line no-eval
      }
      return what
    }
  }
}

if (require.main === module) {
  // instantiate service with its configuration
  new DLake(config)
} else {
  exports.DLake = DLake
}
