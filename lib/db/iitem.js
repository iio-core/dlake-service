const EventEmitter = require('events').EventEmitter

const uuid = require('../utils').uuid

class IItem extends EventEmitter {
  constructor(options) {
    super()

    // makes it abstract
    if (new.target === IItem) {
      throw new TypeError('Cannot construct IItem instances directly')
    }

    // name of the collection as well
    this._name = options.name

    // timeout delay
    this._timeoutDelay = options.timeoutDelay || 5000

    // which roles can access data
    this._authorizedRoles = options.authorizedRoles

    // options for further use
    this._options = options

    // db reference
    this._db = null

    // unique id
    this.uuid = uuid()
  }

  _rawCollectionReady() {
    return new Promise((resolve, reject) => {
      let checkTimeout

      let checkInterval = setInterval(() => {
        if (this._rawCollection) {
          clearInterval(checkInterval)
          clearTimeout(checkTimeout) // nothing if undefined

          resolve(this._rawCollection)
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          reject(new Error('timeout: collection not available'))
        }
      }, this._timeoutDelay)
    })
  }

  ready() {
    return new Promise((resolve, reject) => {
      let checkTimeout

      let checkInterval = setInterval(() => {
        if (this._db && this.$datum && this.$datum.roles &&
            this.$datum.roles._loadedAndReady && this.$datum.users) {
          clearInterval(checkInterval)
          if (checkTimeout) {
            clearTimeout(checkTimeout)
          }
          resolve()
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          reject(new Error('not ready: db is not available'))
        }
      }, 86400000)
    })
  }

  /* get first match: internal */
  _getOne(args) {}

  /* index creation helper */
  createIndex(args) {}

  /* finds matching docs or returns all if empty */
  find(args) {}

  /* finds matching doc for id */
  get(args) {}

  /* updates or inserts a doc if id undefined */
  put(args) {}

  /* deletes doc found thanks to driver related query */
  del(args) {}
}

exports.IItem = IItem
