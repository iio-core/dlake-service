const ObjectID = require('mongodb').ObjectID
const dayjs = require('dayjs')
const _ = require('lodash')

const IItem = require('./iitem').IItem
const utils = require('../utils')
const pino = utils.pino

class ItemMongo extends IItem {
  constructor(options) {
    super(options)

    this._appendOnly = options.appendOnly
    pino.info('initialized datum [' + options.name + '] with options', options)
  }

  /* initialize */
  async _init() {
    try {
      // wait for mongo property ready
      let mongo = await utils.waitForPropertyInit(this.$app, '_mongo')
      // wait for db connection in data center conditions
      await utils.waitForPropertyInit(mongo, '_ready', 86400000)
      // wait for db connected
      this._db = await mongo.waitForDb()
      // get reference to collection as per the db engine
      this._rawCollection = this._db.collection(this._name)
      // REST endpoints
      if (this._options.enableREST) {
        this._createRESTEndpoints()
      }
    } catch (err) {
      pino.error(err, 'mongo db init failure')
    }
  }

  /* get first match: internal */
  _getOne(args) {
    return new Promise((resolve, reject) => {
      this._findOne(args).then(doc => resolve(doc))
        .catch(err => {
          reject(err)
        })
    })
  }

  /* get by id; internal */
  _get(userId) {
    if (typeof userId === 'string') {
      userId = new ObjectID(userId)
    }

    return this._findOne({ _id: userId })
  }

  /* findOne direct call === rawCollection */
  _findOne() {
    return this._rawCollection.findOne.apply(this._rawCollection, Array.from(arguments))
  }

  /* updateOne direct call === rawCollection */
  _updateOne() {
    return this._rawCollection.updateOne.apply(this._rawCollection, Array.from(arguments))
  }

  /* index creation helper */
  createIndex(args) {
    return new Promise((resolve, reject) => {
      this._rawCollection.createIndex(args.index, args.options).then(() => {
        resolve()
      }).catch(err => reject(err))
    })
  }

  /* finds matching docs or returns all if empty qry */
  find(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args || {}
          let options = args.query ? args.options : undefined

          if (args.idFields) {
            for (let field of args.idFields) {
              if (query[field] && typeof query[field] === 'string') {
                query[field] = new ObjectID(query[field])
              } else {
                pino.warn('field [%s] already an ObjectID', field)
              }
            }
          }

          // build ObjectID object from string
          if (query._id && typeof query._id === 'string') {
            query._id = new ObjectID(query._id)
          }

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          this._rawCollection.find(query, options).toArray().then(docs => {
            resolve(docs)
          }).catch(err => {
            pino.error(err, 'find method failed')
            reject(err)
          })
        }).catch(err => {
          pino.error(err, 'find method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'find method failed')
        reject(err)
      })
    })
  }

  /* finds docs and returns sorted list */
  findAndSort(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args || {}
          let options = args.query ? args.options : undefined
          let page = args.page || 1
          let pageSize = args.pageSize || 5000

          // build ObjectID object from string
          if (query._id && typeof query._id === 'string') {
            query._id = new ObjectID(query._id)
          }

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          this._rawCollection.find(query, options)
            .sort(args.sort)
            .skip((page - 1) * pageSize)
            .limit(pageSize).toArray().then(docs => {
              resolve({
                docs: docs,
                total: this._rawCollection.count()
              })
            }).catch(err => {
              pino.error(err, 'find method failed')
              reject(err)
            })
        }).catch(err => {
          pino.error(err, 'find method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'find method failed')
        reject(err)
      })
    })
  }

  /* finds matching docs and returns paginated result */
  findPaginated(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args || {}
          let options = args.query ? args.options : undefined
          let page = args.page || 0
          let pageSize = args.pageSize || 50

          // build ObjectID object from string
          if (query._id && typeof query._id === 'string') {
            query._id = new ObjectID(query._id)
          }

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          this._rawCollection.find(query, options)
            .skip(page * (pageSize - 1))
            .limit(pageSize).toArray().then(docs => {
              resolve({
                docs: docs,
                total: this._rawCollection.count()
              })
            }).catch(err => {
              pino.error(err, 'find method failed')
              reject(err)
            })
        }).catch(err => {
          pino.error(err, 'find method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'find method failed')
        reject(err)
      })
    })
  }

  /* finds matching doc for id or for query */
  get(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args
          let options = args.query ? args.options : undefined

          // build ObjectID object from string
          if (query._id && typeof query._id === 'string') {
            query._id = new ObjectID(query._id)
          }

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          this._findOne(query, options).then(doc => {
            resolve(doc)
          }).catch(err => {
            pino.error(err, 'get method failed')
            reject(err)
          })
        }).catch(err => {
          pino.error(err, 'get method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'get method failed')
        reject(err)
      })
    })
  }

  /* updates or inserts a doc if id undefined */
  put(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        if (args._id && typeof args._id === 'string') {
          args._id = new ObjectID(args._id)
        }

        // updates lastModified
        args._lastModified = new Date()

        if (!this._appendOnly && args._id) {
          this.$datum.roles.permission({
            role: user.role,
            ressource: this._name,
            action: 'updateAny'
          }).then(permission => {
            if (!permission.granted) {
              reject(new Error('access not granted'))
              pino.warn('access not granted', this._name, user.role, permission)
              return
            }

            for (let attr of permission.attributes) {
              if (attr[0] === '!') {
                attr = attr.slice(1)

                if (args[attr]) delete args[attr]
              }
            }

            this._updateOne({
              _id: args._id
            }, { $set: args }, { upsert: true, w: 1 }).then(result => {
              resolve({ _id: args._id })

              // log activity: don't care
              this.$datum.activities._log(userId || 0, this._name + ':update')

              this.emit('update', args._id)
              this.$app._emitPushEvent('data:' + this._name + ':update', args._id)
            }).catch(err => {
              pino.error(err, 'put (update) method failed', args)
              reject(err)
            })
          }).catch(err => {
            pino.error(err, 'put (update) method failed', args)
            reject(err)
          })
        } else {
          this.$datum.roles.permission({
            role: user.role,
            ressource: this._name,
            action: 'createAny'
          }).then(permission => {
            if (!permission.granted) {
              reject(new Error('access not granted'))
              pino.warn('access not granted', this._name, user.role, permission)
              return
            }

            for (let attr of permission.attributes) {
              if (attr[0] === '!') {
                attr = attr.slice(1)

                if (args[attr]) delete args[attr]
              }
            }

            this._rawCollection.insertOne(args, { w: 1 }).then(result => {
              resolve({ _id: result.insertedId })

              // log activity: don't care
              this.$datum.activities._log(userId || 0, this._name + ':create')

              this.emit('add', result.insertedId)
              this.$app._emitPushEvent('data:' + this._name + ':add', result.insertedId)
            }).catch(err => {
              pino.error(err, 'put (create) method failed', args)
              reject(err)
            })
          }).catch(err => {
            pino.error(err, 'put (create) method failed', args)
            reject(err)
          })
        }
      }).catch(err => {
        pino.error(err, 'put method failed', args)
        reject(err)
      })
    })
  }

  /* updates or inserts */
  putsert(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'updateAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query
          let update = args.update

          if (query._id && typeof query._id === 'string') {
            query._id = new ObjectID(query._id)
          }

          // updates lastModified
          update['$set'] = update['$set'] || {}
          update['$set']._lastModified = new Date()

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              if (update['$set'][attr]) delete update['$set'][attr]
            }
          }

          if (!this._appendOnly) {
            this._updateOne(query, update, { upsert: true, w: 1 })
              .then(result => {
                if (result.modifiedCount === 1) {
                  // log activity: don't care
                  this.$datum.activities._log(userId || 0, this._name + ':update')

                  this._rawCollection.findOne(query, { _id: 1 }).then(doc => {
                    resolve({ _id: doc._id })
                    this.emit('update', doc._id)
                    this.$app._emitPushEvent('data:' + this._name + ':update', doc._id)
                  }).catch(err => {
                    pino.error(err, 'missed raising event')
                  })
                } else {
                  reject(new Error('failed to update'))
                }
              }).catch(err => {
                pino.error(err, 'putsert method failed', args)
                reject(err)
              })
          } else {
            reject(new Error('trying to eventually insert when append only'))
          }
        }).catch(err => {
          pino.error(err, 'putsert method failed', args)
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'putsert method failed', args)
        reject(err)
      })
    })
  }

  /* deletes doc found thanks to driver related query */
  del(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'deleteAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          // build ObjectID object from string
          if (args._id) {
            if (typeof args._id === 'string') {
              args._id = new ObjectID(args._id)
            }

            this._rawCollection.findOneAndDelete({ _id: args._id }).then(result => {
              if (result) {
                resolve({ _id: args._id })

                // log activity: don't care
                this.$datum.activities._log(userId || 0, this._name + ':delete')

                this.emit('delete', args._id)
                this.$app._emitPushEvent('data:' + this._name + ':delete', args._id)
              } else {
                reject(new Error('not found'))
              }
            }).catch(err => {
              pino.error(err, 'del method failed')
              reject(err)
            })
          } else {
            this._rawCollection.findOneAndDelete(args).then(result => {
              if (result) {
                resolve({ _id: result._id })

                // log activity: don't care
                this.$datum.activities._log(userId || 0, this._name + ':delete')

                this.emit('delete', result._id)
                this.$app._emitPushEvent('data:' + this._name + ':delete', result._id)
              } else {
                reject(new Error('not found'))
              }
            }).catch(err => {
              pino.error(err, 'del method failed')
              reject(err)
            })
          }
        }).catch(err => reject(err))
      }).catch(err => reject(err))
    })
  }

  /* check if empty */
  isEmpty() {
    return new Promise((resolve, reject) => {
      this._findOne({}).then(doc => {
        if (!doc) {
          resolve({ status: true })
        } else {
          resolve({ status: false })
        }
      }).catch(err => reject(err))
    })
  }

  /* offline sync method */
  sync(args, userId) {
    return new Promise(async (resolve, reject) => {
      if (args.updated && args.updated.length > 0) {
        let updated = _.map(args.updated, e => {
          e._id = new ObjectID(e._id)
          return e
        })

        try {
          for (let e of updated) {
            let doc = await this.get({ _id: e._id }, userId)

            if (dayjs(doc._lastModified).diff(dayjs(e._lastModified)) < 0) {
              e._lastModified = new Date(e._lastModified)
              delete e._notSynched
              await this.put(e, userId)
            }
          }
        } catch (err) {
          reject(err)
          return
        }
      }

      if (args.deleted && args.deleted.length > 0) {
        let deleted = _.map(args.deleted, e => {
          e = new ObjectID(e)
          return e
        })

        try {
          for (let e of deleted) {
            await this.del({ _id: e }, userId)
          }
        } catch (err) {
          reject(err)
        }
      }

      resolve()
    })
  }

  _createRESTEndpoints() {
    let protector = async (req, pathname, version) => {
      try {
        let creds = Buffer.from(req.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
        let username = creds.split(':')[0]
        let password = creds.split(':')[1]

        await this.$datums.users.checkPassword({ username: username, password: password })
        await this.$datums.users.checkPassword({ username: username, password: password })
      } catch (err) {
        return false
      }
      return true
    }

    try {
      this.$app._rest.get({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        pino.info('datum [%s] endpoint [GET] call', this._name)
        try {
          if (request.params.id) {
            let docs = await this._findOne({
              _id: new ObjectID(request.params.id)
            })

            return docs
          } else {
            let query = request.params.q || {}
            let docs = (await this._rawCollection.find(query)).toArray()

            return docs
          }
        } catch (err) {
          return err
        }
      }, { contentType: 'application/json' })

      this.$app._rest.post({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        try {
          let creds = Buffer.from(request.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
          let username = creds.split(':')[0]

          let user = await this.$datum.users.get({ username: username })
          let result = await this.put(request.params.data, user._id)
          return result
        } catch (err) {
          return err
        }
      }, { contentType: 'application/json' })

      this.$app._rest.del({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        try {
          let creds = Buffer.from(request.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
          let username = creds.split(':')[0]
          let password = creds.split(':')[1]

          await this.$datums.users.checkPassword({ username: username, password: password })

          let user = await this.$datum.users.get({ username: username })
          let result = await this.del(request.params.q, user._id)
          return result
        } catch (err) {
          return err
        }
      })

      pino.info('datum [%s] endpoints created', this._name)
    } catch (err) {
      pino.error(err, 'datum [%s] endpoints creation failed', this._name)
    }
  }

  _promiseSerial(funcs) {
    return funcs.reduce((promise, func) =>
      promise.then(result => func().then(Array.prototype.concat.bind(result))),
    Promise.resolve([]))
  }
}

exports.Item = ItemMongo
