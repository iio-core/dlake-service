const _ = require('lodash')

const IItem = require('./iitem').IItem
const utils = require('../utils')
const pino = utils.pino

class ItemCouch extends IItem {
  constructor(options) {
    super(options)

    this._appendOnly = this._options.appendOnly
    pino.info('created datum [' + this._name + '] with options', this._options)
  }

  /* initialize */
  async _init() {
    try {
      // wait for couch property ready
      this._couch = await utils.waitForPropertyInit(this.$app, '_couch')
      // wait for db connection in data center conditions
      await utils.waitForPropertyInit(this._couch, '_ready', 86400000)
      // wait for db connected
      this._db = await this._couch.waitForDb()
      // compatibility
      this._rawCollection = this._db

      // get reference to collection as per the db engine
      try {
        await this._db.view(this._name, 'all')
      } catch (err) {
        await this._db.insert({
          _id: '_design/' + this._name,
          views: {
            all: {
              map: 'function(doc) { \
                if (doc.iio_collection && doc.iio_collection == "' + this._name + '") { \
                  emit(doc._id, doc._rev) \
                }\
              }'
            }
          }
        })
      }

      // REST endpoints
      if (this._options.enableREST) {
        this._createRESTEndpoints()
      }

      pino.info('initialized datum [' + this._options.name + ']')
    } catch (err) {
      pino.error(err, 'couch db init failure')
    }
  }

  /* findOne direct call === rawCollection equivalent for couch */
  _findOne(query) {
    return this._getOne(query)
  }

  /* updateOne direct call === rawCollection */
  _updateOne(query, update) {
    return new Promise((resolve, reject) => {
      query = query || {}
      query.iio_collection = this._name
      this._db.find({ selector: query }).then(doc => {
        doc = _.merge(doc, update.$set)
        this._db.insert(doc).then(() => {
          resolve()
        }).catch(err => reject(err))
      }).catch(err => reject(err))
    })
  }

  /* get first match: internal */
  _getOne(args) {
    return new Promise((resolve, reject) => {
      args = args || {}
      args.iio_collection = this._name
      this._db.find({ selector: args }).then(result => {
        resolve(result.docs[0])
      }).catch(err => {
        if (err.toString().match(/missing/)) resolve()
        else reject(err)
      })
    })
  }

  /* get by id; internal */
  _get(userId) {
    return new Promise((resolve, reject) => {
      if (!userId) {
        resolve()
      } else {
        this._db.get(userId).then(doc => resolve(doc))
          .catch(err => {
            if (err.toString().match(/missing/)) resolve()
            else reject(err)
          })
      }
    })
  }

  /* index creation helper */
  createIndex(args) {
    return new Promise((resolve, reject) => {
      let fields = []
      // let sort = []

      for (let f in args.index) {
        fields.push(f)
        // sort.push({ f: args.index[f] === 1 ? 'asc' : 'dsc' })
      }

      this._db.createIndex({
        name: 'i_base_collection',
        index: {
          fields: [ 'iio_collection' ]
          // sort: sort
        }
      }).catch(err => reject(err))

      this._db.createIndex({
        name: 'i_' + fields.join('_'),
        index: {
          partial_filter_selector: { iio_collection: { $eq: this._name } },
          fields: fields
          // sort: sort
        }
      }).catch(err => reject(err))
    })
  }

  /* finds matching docs or returns all if empty qry */
  find(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args || {}
          let options = args.query ? args.options : undefined

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          let fields = _.map(options.projection, (v, k) => {
            return v ? k : undefined
          })

          this._db.find({
            selector: {
              iio_collection: this._name,
              ...query
            },
            fields: fields
          }).then(result => {
            resolve(result.docs)
          }).catch(err => {
            pino.error(err, 'find method failed')
            reject(err)
          })
        }).catch(err => {
          pino.error(err, 'find method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'find method failed')
        reject(err)
      })
    })
  }

  /* finds matching docs and paginates them as per args request */
  findPaginated(args, userId) {
    return new Promise((resolve, reject) => {
      this.find(args, userId).then(result => {
        resolve(result)
      }).catch(err => reject(err))
    })
  }

  /* finds matching docs and sorts them */
  findAndSort(args, userId) {
    return new Promise((resolve, reject) => {
      this.find(args, userId).then(result => {
        if (args.sort) {
          let fields = []
          let sort = []
          for (let s in args.sort) {
            fields.push(s)
            sort.push(args.sort[s] === 1 ? 'asc' : 'desc')
          }
          let docs = _.orderBy(result.docs, fields, sort)
          resolve(docs)
        } else {
          resolve(result)
        }
      }).catch(err => reject(err))
    })
  }

  /* finds matching doc for id or for query */
  get(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'readAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          let query = args.query || args
          let options = args.query ? args.options : undefined

          options = options || {}
          options.projection = options.projection || {}

          for (let attr of permission.attributes) {
            if (attr[0] === '!') {
              attr = attr.slice(1)

              options.projection[attr] = 0
            } else {
              if (attr !== '*') {
                options.projection[attr] = 1
              }
            }
          }

          let fields = _.map(options.projection, (v, k) => {
            return v ? k : undefined
          })

          query.iio_collection = this._name
          this._db.find({
            selector: query,
            fields: fields
          }).then(result => {
            resolve(result.docs[0])
          }).catch(err => {
            pino.error(err, 'get method failed')
            reject(err)
          })
        }).catch(err => {
          pino.error(err, 'get method failed')
          reject(err)
        })
      }).catch(err => {
        pino.error(err, 'get method failed')
        reject(err)
      })
    })
  }

  /* updates or inserts a doc if id undefined */
  put(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        // updates lastModified
        args.lastModified = new Date()

        if (!this._appendOnly && args._id) {
          this.$datum.roles.permission({
            role: user.role,
            ressource: this._name,
            action: 'updateAny'
          }).then(permission => {
            if (!permission.granted) {
              reject(new Error('access not granted'))
              pino.warn('access not granted', this._name, user.role, permission)
              return
            }

            for (let attr of permission.attributes) {
              if (attr[0] === '!') {
                attr = attr.slice(1)

                if (args[attr]) delete args[attr]
              }
            }

            args.iio_collection = this._name
            this._db.insert(args).then(response => {
              args._rev = response.rev
              let result = _.cloneDeep(args)
              result._updated = {
                ops: [ args ]
              }
              resolve(result)

              this.emit('update', args._id)
            }).catch(err => {
              pino.error(err, 'put (update) method failed', args)
              reject(err)
            })
          }).catch(err => {
            pino.error(err, 'put (update) method failed', args)
            reject(err)
          })
        } else {
          this.$datum.roles.permission({
            role: user.role,
            ressource: this._name,
            action: 'createAny'
          }).then(permission => {
            if (!permission.granted) {
              reject(new Error('access not granted'))
              pino.warn('access not granted', this._name, user.role, permission)
              return
            }

            for (let attr of permission.attributes) {
              if (attr[0] === '!') {
                attr = attr.slice(1)

                if (args[attr]) delete args[attr]
              }
            }

            this._couch._client.uuids(1).then(doc => {
              args._id = doc.uuids[0]
              args.iio_collection = this._name
              this._db.insert(args).then(response => {
                args._rev = response.rev
                let result = _.cloneDeep(args)
                result._updated = {
                  ops: [ args ]
                }
                resolve(result)

                this.emit('add', args)
              }).catch(err => {
                pino.error(err, 'put (create) method failed', args)
                reject(err)
              })
            }).catch(err => {
              pino.error(err, 'put (create) method failed: failed to get uuid', args)
              reject(err)
            })
          }).catch(err => {
            pino.error(err, 'put (create) method failed', args)
            reject(err)
          })
        }
      }).catch(err => {
        pino.error(err, 'put method failed', args)
        reject(err)
      })
    })
  }

  /* deletes doc found thanks to driver related query */
  del(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        user = user || { role: 'anonymous' }

        this.$datum.roles.permission({
          role: user.role,
          ressource: this._name,
          action: 'deleteAny'
        }).then(permission => {
          if (!permission.granted) {
            reject(new Error('access not granted'))
            pino.warn('access not granted', this._name, user.role, permission)
            return
          }

          // build ObjectID object from string
          if (args._id) {
            this._db.destroy(args._id, args._rev).then(result => {
              if (result) {
                resolve({ _updated: result })

                this.emit('delete', result)
              } else {
                reject(new Error('not found ' + JSON.stringify(result, null, 2)))
              }
            }).catch(err => {
              pino.error(err, 'del method failed')
              reject(err)
            })
          } else {
            pino.error('del method failed: missing id')
            reject(new Error('missing id'))
          }
        }).catch(err => reject(err))
      }).catch(err => reject(err))
    })
  }

  /* check if empty */
  isEmpty() {
    return new Promise((resolve, reject) => {
      this._db.view(this._name, 'all').then(result => {
        if (!result.rows || result.rows.length === 0) {
          resolve({ status: true })
        } else {
          resolve({ status: false })
        }
      }).catch(err => {
        pino.error('isEmpty ' + !!err)
        resolve({ status: false })
      })
    })
  }

  _createRESTEndpoints() {
    let protector = async (req, pathname, version) => {
      try {
        let creds = Buffer.from(req.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
        let username = creds.split(':')[0]
        let password = creds.split(':')[1]

        await this.$datums.users.checkPassword({ username: username, password: password })
      } catch (err) {
        return false
      }
      return true
    }

    try {
      this.$app._rest.get({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        pino.info('datum [%s] endpoint [GET] call', this._name)
        try {
          if (request.params.id) {
            let docs = await this._db.get(request.params.id)
            return docs
          } else {
            let query = request.params.q || {}
            let docs = (await this._db.find({
              selector: query
            }))

            return docs
          }
        } catch (err) {
          return err
        }
      }, { contentType: 'application/json' })

      this.$app._rest.post({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        try {
          let creds = Buffer.from(request.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
          let username = creds.split(':')[0]

          let user = await this.$datum.users.get({ username: username })
          let result = await this.insert(request.params.data, user._id)
          return result
        } catch (err) {
          return err
        }
      }, { contentType: 'application/json' })

      this.$app._rest.del({
        path: '/' + this._name + '/?id',
        unprotected: true,
        protector: protector,
        context: '/api'
      }, async (request, content) => {
        try {
          let creds = Buffer.from(request.headers.authorization.replace('Basic ', ''), 'base64').toString('utf8')
          let username = creds.split(':')[0]

          let user = await this.$datum.users.get({ username: username })
          let result = await this.del(request.params.q, user._id)
          return result
        } catch (err) {
          return err
        }
      })

      pino.info('datum [%s] endpoints created', this._name)
    } catch (err) {
      pino.error(err, 'datum [%s] endpoints creation failed', this._name)
    }
  }

  _promiseSerial(funcs) {
    return funcs.reduce((promise, func) =>
      promise.then(result => func().then(Array.prototype.concat.bind(result))),
    Promise.resolve([]))
  }
}

exports.Item = ItemCouch
