const MongoClient = require('mongodb').MongoClient

const pino = require('./utils').pino

class Mongo {
  constructor(options) {
    this._options = options
    this._db = null

    let attempts = 0

    let connInterval = setInterval(async () => {
      if (await this._connect(attempts)) {
        clearInterval(connInterval)
        this._ready = true
        return
      }

      attempts++

      if (attempts >= this._options.maxAttempts) {
        clearInterval(connInterval)
        pino.error('error connecting to db')
        process.exit(1)
      }
    }, 1000)
  }

  async _connect(attempts) {
    try {
      var uri

      if (this._options.user) {
        uri = 'mongodb+srv://' + this._options.user + ':' +
          this._options.password + '@' + this._options.uri +
          '/' + this._options.dbName +
          (this._options.options ? '?' + this._options.options : '')
      } else {
        uri = this._options.uri +
          '/' + this._options.dbName +
          (this._options.options ? '?' + this._options.options : '')
      }

      this._client = await MongoClient.connect(uri, { useNewUrlParser: true })

      this._db = this._client.db()
      return true
    } catch (err) {
      pino.warn('Waiting for db... ' + attempts + 's elapsed at ' + uri)
      return false
    }
  }

  get db() {
    return this._db
  }

  waitForDb() {
    return new Promise((resolve, reject) => {
      let checkTimeout

      let checkInterval = setInterval(() => {
        if (this._db) {
          clearInterval(checkInterval)
          clearTimeout(checkTimeout) // nothing if undefined

          resolve(this._db)
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          reject(new Error('Timeout: db is not available'))
        }
      }, this._options.maxAttempts * 1000)
    })
  }
}

exports.Mongo = Mongo
