const path = require('path')

exports.Item = (() => {
  let dbEngine = require(path.join(process.cwd(), 'config')).db.engine
  return require('../db/item-' + dbEngine).Item
})()
