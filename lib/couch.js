const nano = require('nano')

const pino = require('./utils').pino

class Couch {
  constructor(options) {
    this._options = options
    this._db = null

    let attempts = 0

    let connInterval = setInterval(async () => {
      if (await this._connect(attempts)) {
        clearInterval(connInterval)
        this._ready = true
        return
      }

      attempts++

      if (attempts >= this._options.maxAttempts) {
        clearInterval(connInterval)
        pino.error('error connecting to db')
        process.exit(1)
      }
    }, 1000)
  }

  _dbExists(name) {
    return new Promise((resolve, reject) => {
      let found
      this._client.db.list().then(body => {
        for (let db of body) {
          if (db === name) {
            found = true
            break
          }
        }

        resolve(found)
      }).catch(err => {
        reject(err)
      })
    })
  }

  async _connect(attempts) {
    try {
      var uri = this._options.uri +
        '/' + (this._options.options ? '?' + this._options.options : '')
      this._client = await nano(uri)
      let dbExists = await this._dbExists(this._options.dbName)
      if (!dbExists) {
        await this._client.db.create(this._options.dbName)
      }
      this._db = await this._client.db.use(this._options.dbName)
      return true
    } catch (err) {
      pino.warn('Waiting for db... ' + attempts + 's elapsed at ' + uri)
      return false
    }
  }

  get db() {
    return this._db
  }

  waitForDb() {
    return new Promise((resolve, reject) => {
      let checkTimeout

      let checkInterval = setInterval(() => {
        if (this._db) {
          clearInterval(checkInterval)
          clearTimeout(checkTimeout) // nothing if undefined

          resolve(this._db)
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          reject(new Error('Timeout: db is not available'))
        }
      }, this._options.maxAttempts * 1000)
    })
  }
}

exports.Couch = Couch
