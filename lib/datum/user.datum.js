const _ = require('lodash')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const ObjectID = require('mongodb').ObjectID

const Item = require('../core/item').Item
const pino = require('../utils').pino

/*
 * {
 *   email: 'vdrac@free.fr',
 *   username: 'ignitial',
 *   password: hash
 * }
 */
class User extends Item {
  constructor(options) {
    options.name = 'users'
    super(options)

    this.ready().then(() => {
      this.createIndex({
        index: { 'username': 1 },
        options: { unique: true }
      }).catch(err => {
        pino.error(err, 'failed to create index for ' + this._name)
      })
    }).catch(err => pino.error(err, 'failed to create collection ' + this._name))
  }

  /* updates only his own data for current user */
  updateMine(args, userId) {
    return new Promise((resolve, reject) => {
      this._get(userId).then(user => {
        if (user) {
          this.$datum.roles.permission({
            role: user.role,
            ressource: 'roles',
            action: 'updateOwn'
          }).then(permission => {
            if (permission.granted) {
              // updates only if user matches logged one
              if (args.username === user.username) {
                for (let attr of permission.attributes) {
                  if (attr[0] === '!') {
                    attr = attr.slice(1)

                    if (args[attr]) delete args[attr]
                  }
                }

                if (args._id && typeof args._id === 'string') {
                  try {
                    args._id = new ObjectID(args._id)
                  } catch (err) {
                    pino.warn('not mongo id')
                  }
                } else {
                  reject(new Error('trying to overwrite access check'))
                  return
                }

                this._updateOne({
                  _id: args._id
                }, { $set: args }, { upsert: true, w: 1 }).then(result => {
                  resolve({ _updated: result })

                  // log activity: don't care
                  this.$datum.activities._log(userId || 0, this._name + ':update')

                  this.emit('update', args._id)
                  this.$app._emitPushEvent('data:users:update', args._id)
                }).catch(err => {
                  pino.error(err, 'put (update) method failed', args)
                  reject(err)
                })
              } else {
                reject(new Error('user does not matched logged one'))
              }
            }
          }).catch(err => reject(err))
        } else {
          reject(new Error('user not defined'))
        }
      }).catch(err => {
        reject(err)
      })
    })
  }

  // updates everything but password (only his own data) */
  updateMineExceptCred(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(async user => {
        if (user) {
          let all = [
            this.$datum.roles.permission({
              role: user.role,
              ressource: 'roles',
              action: 'updateOwn'
            }),
            this.$datum.roles.permission({
              role: user.role,
              ressource: 'roles',
              action: 'updateAny'
            })
          ]

          Promise.all(all).then(permission => {
            if ((permission[0].granted && args.username === user.username) ||
              permission[1].granted) {
              if (permission[0].granted) {
                for (let attr of permission[0].attributes) {
                  if (attr[0] === '!') {
                    attr = attr.slice(1)

                    if (args[attr]) delete args[attr]
                  }
                }
              }

              let id = args._id
              try {
                id = new ObjectID(id)
              } catch (err) {
                pino.warn('not mongo id')
              }

              this._updateOne({ _id: id },
                { $set: _.omit(args, [ '_id', 'password' ]) })
                .then(result => {
                  resolve(result)

                  // log activity: don't care
                  this.$datum.activities._log(userId || 0, this._name + ':update')

                  this.emit('update', id)
                  this.$app._emitPushEvent('data:users:update', id)
                }).catch(err => {
                  reject(err)
                })
            } else {
              reject(new Error('access not granted'))
            }
          }).catch(err => reject(err))
        } else {
          reject(new Error('user not defined'))
        }
      }).catch(err => {
        reject(err)
      })
    })
  }

  putNewUser(args, userId) {
    return new Promise(async (resolve, reject) => {
      this.$datum.roles.permissionById({
        ressource: 'roles',
        action: 'createAny'
      }, userId).then(permission => {
        if (permission.granted) {
          try {
            let user = args.user || args

            let password = Math.random().toString(36).slice(2, 12)
            let salt = bcrypt.genSaltSync(10)
            let hash = bcrypt.hashSync(password, salt)

            user.password = hash

            this.put(user, userId).then(result => {
              this.get({ _id: result._id }).then(doc => {
                resolve({ doc: doc })
              }).catch(err => reject(err))

              this.$app._sendMail({
                to: user.contactInfo.email,
                subject: this.$app._i18n(user.lang, 'DO NOT ANSWER') + ' - ' +
                  this.$app._i18n(user.lang, 'Account creation'),
                text:
                  this.$app._i18n(user.lang, 'An account has been created for you') +
                  '\n' + 'username: ' + user.username + ', password: ' + password,
                html:
                  '<p>' +
                  this.$app._i18n(user.lang, 'An account has been created for you') +
                  '\n' + 'username: ' + user.username + ', password: ' + password + '</p>'
              }).catch(err => pino.error(err, 'mail send failed'))
            }).catch(err => reject(err))
          } catch (err) {
            reject(err)
          }
        } else {
          reject(new Error('access not granted'))
        }
      }).catch(err => reject(err))
    })
  }

  checkPassword(args, userId) {
    return new Promise((resolve, reject) => {
      this._findOne({ 'username': args.username }).then(user => {
        if (user) {
          if (bcrypt.compareSync(args.password, user.password)) {
            args.password = user.password
            args._id = user._id
            delete user.password

            let response = {
              user: user,
              token: jwt.sign(args, this._options.auth.secret, {
                expiresIn: this._options.auth.tokenTimeout || '5h'
              })
            }

            resolve(response)
          } else {
            reject(new Error('wrong password'))
          }
        } else {
          reject(new Error('user not found'))
        }
      }).catch(err => {
        console.log(err)
        reject(new Error('sign in failed'))
      })
    })
  }

  checkToken(args, userId) {
    return new Promise((resolve, reject) => {
      if (args.token) {
        try {
          jwt.verify(args.token, this._options.auth.secret, (err, decoded) => {
            if (err) {
              reject(err)
            } else {
              resolve(decoded)
            }
          })
        } catch (err) {
          reject(err)
          console.log(err)
        }
      } else {
        reject(new Error('token missing'))
      }
    })
  }
}

exports.User = User
