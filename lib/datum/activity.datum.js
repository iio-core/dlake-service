const Item = require('../core/item').Item
const dayjs = require('dayjs')

const pino = require('../utils').pino

/*

  {
    "user": "....",
    "initDate": Date,
    "daily": [
      [
        {
          "date": ...,
          "log": "..."
        },
        {
          "date": ...,
          "log": "..."
        }
      ],
      [
        {
          "date": ...,
          "log": "..."
        }
      ]
    ]
  }

*/
class Activity extends Item {
  constructor(options) {
    options.name = 'activities'
    super(options)

    this.ready().then(() => {
      this.createIndex({
        index: { 'user': 1 },
        options: { unique: true }
      }).catch(err => {
        pino.error(err, 'failed to create index for ' + this._name)
      })
    })
  }

  _log(userId, activity) {
    this._findOne({ user: userId }).then(doc => {
      doc = doc || {
        user: userId,
        initDate: new Date(),
        daily: []
      }

      let day = function(d) {
        d = d || new Date()
        return dayjs(dayjs(d).startOf('day').format()).startOf('day').toDate()
      }

      let index = dayjs(day()).diff(dayjs(day(doc.initDate)), 'days')
      let delta = index - doc.daily.length

      if (delta > 0) {
        doc.daily = doc.daily.concat(new Array(delta))
        doc.daily[index] = []
      } else if (delta === 0) {
        doc.daily.push([])
      }

      doc.daily[index].push({
        date: new Date(),
        log: activity
      })

      this._updateOne({ user: doc.user }, { $set: doc }, { upsert: true })
        .catch(err => {
          pino.error(err, 'failed to log activity')
        })
    }).catch(err => {
      pino.error(err, 'failed to log activity')
    })
  }

  obtainUserActivity(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(async user => {
        let isAdmin = await this.$datum.admins.get({ username: user.username })

        if (isAdmin || userId === args.user) {
          this.get({ user: args.user }).then(doc => {
            resolve(doc)
          }).catch(err => {
            reject(err)
          })
        } else {
          reject(new Error('not allowed'))
        }
      }).catch(err => {
        reject(err)
      })
    })
  }
}

exports.Activity = Activity
