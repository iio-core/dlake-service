const Item = require('../core/item').Item

/*  */
class Meta extends Item {
  constructor(options) {
    options.name = 'metas'
    super(options)
  }
}

exports.Meta = Meta
