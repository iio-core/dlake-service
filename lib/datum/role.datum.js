const AccessControl = require('accesscontrol')

const Item = require('../core/item').Item

const pino = require('../utils').pino

/*
{
    admin: {
        video: {
            'create:any': ['*', '!views'],
            'read:any': ['*'],
            'update:any': ['*', '!views'],
            'delete:any': ['*']
        }
    },
    user: {
        video: {
            'create:own': ['*', '!rating', '!views'],
            'read:own': ['*'],
            'update:own': ['*', '!rating', '!views'],
            'delete:own': ['*']
        }
    }
}
*/
class Role extends Item {
  constructor(options) {
    options.name = 'roles'
    super(options)

    this._ac = new AccessControl({})

    this._loadedAndReady = false

    this._rawCollectionReady().then(() => {
      this.load().then(() => {
        this._loadedAndReady = true
        this.on('update', this.load)
      }).catch(err => {
        pino.error(err, 'unable to load roles')
      })
    }).catch(err => {
      pino.error(err, 'unable to load roles: collectio not ready')
    })
  }

  load() {
    // @_GET_
    return new Promise((resolve, reject) => {
      this._getOne({}).then(doc => {
        if (doc) {
          try {
            delete doc._id
            delete doc._rev // couch
            delete doc.iio_collection // couch
            this._ac.setGrants(doc)
            resolve(doc)
            pino.warn('Roles (re)load')
          } catch (err) {
            console.log(err)
            reject(new Error('grants error'))
          }
        } else {
          reject(new Error('no roles defined'))
        }
      }).catch(err => reject(err))
    })
  }

  save(args, userId) {
    // @_POST_
    return new Promise((resolve, reject) => {
      this.$datum.users._get(userId).then(user => {
        if (user) {
          this.permission({
            role: user.role,
            ressource: 'roles',
            action: 'updateAny'
          }).then(permission => {
            if (permission.granted) {
              this._save.then(() => {
                resolve()
              }).catch(err => reject(err))
            } else {
              reject(new Error('access not granted'))
            }
          }).catch(err => reject(err))
        } else {
          reject(new Error('access not granted'))
        }
      }).catch(err => reject(err))
    })
  }

  permissionById(args, userId) {
    // @_GET_
    return new Promise((resolve, reject) => {
      if (!args) {
        reject(new Error('attempt to fill empty roles'))
        return
      }

      this.$datum.users._get(userId).then(user => {
        if (user) {
          args.role = user.role
          this.permission(args).then(permission => {
            resolve(permission)
          }).catch(err => reject(err))
        } else {
          reject(new Error('access not granted'))
        }
      }).catch(err => reject(err))
    })
  }

  permission(args) {
    // @_GET_
    return new Promise((resolve, reject) => {
      if (!args) {
        reject(new Error('attempt to get info for empty role'))
        return
      }

      try {
        let p = this._ac.can(args.role)[args.action](args.ressource)
        resolve(p)
      } catch (err) {
        reject(err)
      }
    })
  }

  _save() {
    return new Promise((resolve, reject) => {
      this._findOne({}).then(doc => {
        let grants = this._ac.getGrants()
        this._updateOne({
          _id: doc._id
        }, {
          $set: grants
        }).then(() => {
          pino.warn('roles saved after modification')
          resolve()
        }).catch(err => reject(err))
      }).catch(err => reject(err))
    })
  }
}

exports.Role = Role
