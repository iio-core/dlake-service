const Item = require('../core/item').Item
const pino = require('../utils').pino

/* */
class Notification extends Item {
  constructor(options) {
    options.name = 'notifications'
    super(options)

    this.ready().then(() => {
      this.createIndex({
        index: { 'user': 1 },
        options: { unique: false }
      }).catch(err => {
        pino.error(err, 'failed to create index for ' + this._name)
      })
    }).catch(err => pino.error(err, 'failed to create collection ' + this._name))

    this.on('add', this._handleNewNotification)
  }

  _handleNewNotification(notification) {
    this.$app._emitPushEvent('notification:add', notification)
  }

  obtainMine(args, userId) {
    return new Promise((resolve, reject) => {
      this.$datum.users.get({ _id: userId }, userId).then(user => {
        this.find({
          'user': user.username
        }, userId).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      }).catch(err => {
        reject(err)
      })
    })
  }
}

exports.Notification = Notification
