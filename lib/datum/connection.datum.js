const ObjectID = require('mongodb').ObjectID

const Item = require('../core/item').Item
const pino = require('../utils').pino

/* */
class Connection extends Item {
  constructor(options) {
    options.name = 'connections'
    super(options)

    this._appendOnly = true

    this.ready().then(() => {
      this.createIndex({
        index: { user: 1, date: -1 },
        options: { unique: false }
      }).catch(err => {
        pino.error(err, 'failed to create index for ' + this._name)
      })
    }).catch(err => pino.error(err, 'failed to create collection ' + this._name))
  }

  /* provides latest record for an user */
  latest(args, userId) {
    return new Promise((resolve, reject) => {
      if (typeof args.user === 'string') {
        args.user = new ObjectID(args.user)
      }

      this._rawCollection.find({ user: args.user }).sort({ date: -1 }).limit(1)
        .then(docs => {
          if (docs && docs[0]) {
            resolve({ latest: docs[0] })
          } else {
            reject(new Error('no record found'))
          }
        }).catch(err => {
          reject(err)
        })
    })
  }

  signInLog(args, userId) {
    return new Promise((resolve, reject) => {
      let id = userId
      try {
        id = new ObjectID(userId)
      } catch (err) {
        pino.warn('not mongo id')
      }

      this.$datum.users._updateOne({
        _id: id
      }, {
        $set: { logged: true }
      }).then(() => {
        this.put({
          user: id,
          date: new Date(),
          type: 'signin'
        }, userId).then(() => {
          resolve()
        }).catch(err => {
          reject(err)
        })
      }).catch(err => {
        reject(err)
      })
    })
  }

  signOutLog(args, userId) {
    return new Promise((resolve, reject) => {
      let id = userId
      try {
        id = new ObjectID(userId)
      } catch (err) {
        pino.warn('not mongo id')
      }

      this.$datum.users._updateOne({
        _id: id
      }, {
        $set: { logged: false }
      }).then(() => {
        this.put({
          user: id,
          date: new Date(),
          type: 'signout'
        }, userId).then(() => {
          resolve()
        }).catch(err => reject(err))
      }).catch(err => {
        reject(err)
      })
    })
  }
}

exports.Connection = Connection
