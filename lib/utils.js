const pino = require('pino')

exports.pino = pino({
  name: 'dlake-service',
  safe: true,
  level: process.env.LOG_LEVEL || 'warn',
  prettyPrint: { colorize: true }
})

exports.uuid = () => {
  return Math.random().toString(36).slice(2)
}

/* wait for obj property to be defined */
exports.waitForPropertyInit = (obj, name, delay) => {
  delay = delay || 5000

  return new Promise((resolve, reject) => {
    let checkTimeout

    let checkInterval = setInterval(() => {
      if (obj[name] !== undefined) {
        clearInterval(checkInterval)
        clearTimeout(checkTimeout) // nothing if undefined

        resolve(obj[name])
      }
    }, 100)

    checkTimeout = setTimeout(() => {
      if (checkInterval) {
        clearInterval(checkInterval)
        reject(new Error('timeout: [' + name + '] property is not available'))
      }
    }, delay)
  })
}
