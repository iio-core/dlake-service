FROM node:10-alpine

RUN npm install pm2 -g

RUN mkdir -p /opt && mkdir -p /opt/dlake

ADD . /opt/dlake

WORKDIR /opt/dlake

RUN npm install && npm run build:dev

CMD ["pm2-docker", "index.js"]
