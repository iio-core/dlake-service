#!/bin/sh

export MONGODB_URI=mongodb://127.0.0.1:40000
docker-compose -f docker-compose.yml up -d
sleep 2
node test/if-test-mongo.js

docker-compose stop
docker-compose rm -f
export MONGODB_URI=
