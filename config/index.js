const fs = require('fs')

// HTTP server configuration
// -----------------------------------------------------------------------------
const IIOS_SERVER_PORT = process.env.NODE_ENV === 'production' ?
  8080 :
  process.env.IIOS_SERVER_PORT || 4093

const IIOS_SERVER_HOST = process.env.IIOS_SERVER_HOST || '127.0.0.1'
const IIOS_SERVER_PATH_TO_SERVE = process.env.IIOS_SERVER_PATH_TO_SERVE || './dist'

// REDIS configuration
// -----------------------------------------------------------------------------
const REDIS_HOST = process.env.REDIS_HOST || '127.0.0.1'
const REDIS_PORT = process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379
const REDIS_DB = process.env.REDIS_DB || 0
let REDIS_SENTINELS

if (process.env.REDIS_SENTINELS) {
  REDIS_SENTINELS = []
  let sentinels = process.env.REDIS_SENTINELS.split(',')
  for (let s of sentinels) {
    REDIS_SENTINELS.push({ host: s.split(':')[0], port: s.split(':')[1] })
  }
}

// HTTP REST API key and context for restricted access
// -----------------------------------------------------------------------------
const REST_API_KEY = process.env.REST_API_KEY || '2bpukqziosbejet2k9duvpadajsfrm4u'
const REST_API_CONTEXT = process.env.REST_API_CONTEXT || '/api'

// Authentication secrets
const AUTH_SECRET = process.env.AUTH_SECRET || 'Once upon the time, for ever'

// MONGO access secrets
var MONGODB_PASSWORD = process.env.MONGODB_PASSWORD

// get from docker secrets
try {
  MONGODB_PASSWORD = fs.readFileSync('/run/secrets/mongodb_pwd', 'utf8').replace('\n', '')
} catch (err) {
  console.log('config: no pwd secret defined')
}

// SMTP secrets
var EMAILER_SMTP_PASS = process.env.EMAILER_SMTP_PASS

// get from docker secrets
if (!EMAILER_SMTP_PASS) {
  try {
    EMAILER_SMTP_PASS = fs.readFileSync('/run/secrets/emailer_smtp_pass', 'utf8').replace('\n', '')
  } catch (err) {}
}

// Main configuration structure
// -----------------------------------------------------------------------------
module.exports = {
  /* name */
  name: process.env.DLAKE_NAME || 'dlake',
  /* service namesapce */
  namespace: process.env.IIOS_NAMESPACE || 'ignitialio',
  /* heartbeat */
  heartbeatPeriod: 5000,
  /* redis server connection */
  redis: {
    master: process.env.REDIS_MASTER_NAME,
    sentinels: REDIS_SENTINELS,
    host: REDIS_HOST,
    port: REDIS_PORT,
    db: REDIS_DB
  },
  /* db engine */
  db: {
    engine: process.env.DB_ENGINE || 'mongo',
    enableREST: true
  },
  /* mongodb */
  mongo: {
    uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:40000',
    dbName: process.env.MONGODB_DBNAME || 'ignitialio',
    options: process.env.MONGODB_OPTIONS,
    maxAttempts: process.env.MONGODB_CONN_MAX_ATTEMPTS || 30,
    user: process.env.MONGODB_USER,
    password: MONGODB_PASSWORD
  },
  /* couchdb */
  couch: {
    uri: process.env.COUCHDB_URI || 'http://127.0.0.1:5984',
    dbName: process.env.COUCHDB_DBNAME || 'ignitialio',
    options: process.env.COUCHDB_OPTIONS,
    maxAttempts: process.env.COUCHDB_CONN_MAX_ATTEMPTS || 30
  },
  /* datum definition pathes */
  datum: {
    paths: [ './lib/datum', '/opt/dlake/datum' ],
    configPath: './config'
  },
  /* HTTP server declaration */
  server: {
    /* server host */
    host: IIOS_SERVER_HOST,
    /* server port */
    port: IIOS_SERVER_PORT,
    /* path to statically serve (at least one asset for icons for example) */
    path: IIOS_SERVER_PATH_TO_SERVE
  },
  /* see connect-rest */
  rest: {
    context: REST_API_CONTEXT,
    apiKeys: [ REST_API_KEY ]
  },
  /* authentication secrets and timeout */
  auth: {
    secret: AUTH_SECRET,
    jwtTimeout: '5h'
  },
  /* options published through discovery mechanism */
  publicOptions: {
    /* declares component injection */
    uiComponentInjection: true,
    /* service description */
    description: {
      /* service icon */
      icon: 'assets/dlake-64.png',
      /* Internationalization: see Ignitial.io Web App */
      i18n: {
        'Data lake for micro-services': [ 'Lac de données pour micro-services', 'Lago de datos para microservicios' ],
        'Data access from any service':  [ 'Accès aux données à partir de n\'importe quel service',
          'Acceso a datos desde cualquier servicio'],
        'Add a new ressource': [ 'Rajouter une nouvelle ressource', 'Añadir un nuevo recurso' ],
        'Add a new role': [ 'Rajouter un nouveau rôle', 'Añadir un nuevo rol' ],
        'Add a new access': [ 'Rajouter un nouvel accès', 'Añadir nuevo acceso' ]
      },
      /* eventually any other data */
      title: 'Data lake for micro-services',
      info: 'Data access from any service'
    }
  },
  /* emailer configuration for email notifications */
  emailer: {
    smtp: {
      host: process.env.EMAILER_SMTP_HOST || 'mail.ignitial.fr', /* SMTP host */
      port: process.env.EMAILER_SMTP_PORT || 25, /* SMTP port */
      secure: process.env.EMAILER_SMTP_SECURE === 'true', /* true for 465, false for other ports */
      auth: {
        user: process.env.EMAILER_SMTP_USER, /* SMTP server user account */
        pass: EMAILER_SMTP_PASS /* SMTP server user password */
      },
      logger: false,
      debug: false
    },
    mail: {
      from: 'Systra Support <no-reply@ignitial.fr>'
    }
  },
  /* server side translations */
  i18n: {
    lang: [ 'en-US', 'fr-FR', 'es-ES' ],
    translations: {
      'DO NOT ANSWER': [ 'NE PAS RÉPONDRE', 'NO CONTESTAR' ],
      'Account creation': [ 'Création de compte', 'Creación de cuenta' ],
      'An account has been created for you': [ 'Un compte a été créé pour vous', 'Se ha creado una cuenta para usted']
    }
  }
}
